# If you have problems with gflags_catkin

Perform `git clone https://github.com/ethz-asl/gflags_catkin.git`
Then in the CmaleLists.txt of it change the following line
`GIT_REPOSITORY https://code.google.com/p/gflags/` to
`GIT_REPOSITORY  https://github.com/gflags/gflags.git`

# Phone camera internal Calibration data format
1. 32 bit float in BigEndian format
2. Order is k_x, gamma, c_x, k_y, c_y

# Phone camera external calibration data format
1. 32 bit float in BigEndian format
2. Order is R11, R12, R13, t1, R21, R22, R23, t2, R31, R32, R33, t3 => first three rows of the 4X4 transformation matrix.

# Marker data format
1. TimeStamp, C_row,        C_col

2. long int,  32 bit float  32 bit float