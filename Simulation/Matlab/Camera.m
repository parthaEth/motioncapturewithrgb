% Performas camera projection operations. Pin-hole model with different
% distortion functions.
classdef Camera < handle
    properties
        f_x_ = 320;
        f_y_ = 320;
        
        im_rows_ = 480;
        im_cols_ = 640;
        
        c_x_ = 240;
        c_y_ = 320;
        
        % This is a 7x1 matrix. First 3 entries are the position components
        % of the camera in global frame (p_G) and the next 4 entries make a
        % hamiltonian active rotation quaternion (q_B_G).
        camera_pose_ = zeros(7, 1);
        
        time_of_capture_ = 0;
        
        pixel_measurement_noise_amplitude_ = 0.9; %pixel
        time_of_capture_measurement_noise_ = 0.005; %second
    end
    
    methods
        function this = Camera(f_x, f_y, im_rows, im_cols, c_x, c_y, pose)
            if(nargin < 7)
                this.camera_pose_(4, 1) = 1;
                return;
            end
            this.f_x_ = f_x;
            this.f_y_ = f_y;
            
            this.im_rows_ = im_rows;
            this.im_cols_ = im_cols;
            
            this.c_x_ = c_x;
            this.c_y_ = c_y;
            
            if(length(pose) ~= 7)
                error('Unknown pose format.');
            end
            
            if(abs(norm(pose(4:7)) - 1) > 1e-4)
                error(['Pose quaternion must be of unit norm. Provided ', ...
                    num2str(norm(pose(4:7)))] );
            end
            
            this.camera_pose_ = pose;
        end
        
        function [p_C, is_behind] = transform3d(this, p_G)
            is_behind = false;
            p_C = p_G - repmat(this.camera_pose_(1:3, 1), 1, size(p_G, 2));
            R = quat2rotm(this.camera_pose_(4:7, 1)');
            p_C = R * p_C;
            if p_C(3, 1) < 0
                is_behind = true;
            end
        end
        
        function [u_v_C, status] = project3d(this, p_G)
            % -----status codes-------
            % status = 0 : No error
            % status = 1 : Point behind camera
            % status = 2 : Projection out of image dimension
            % status = 3 : Unknown error
            % returned values in u_v_C must not be used if the status code
            % is anything other than 0 or 2. In case of status code 2
            % expect NaN-s in the returned value.
            status = 0;
            
            [p_C, is_behind] = this.transform3d(p_G);
            if(is_behind)
                u_v_C = zeros(2, size(p_C, 2));
                status = 1;
                return;
            end
            
            K = eye(3);
            K(1, 1) = this.f_x_;
            K(2, 2) = this.f_y_;
            K(1, 3) = this.c_x_;
            K(2, 3) = this.c_y_;
            
            p_C = K * p_C;
            u_v_C = p_C(1:2, :) ./ repmat(p_C(3, :), 2 , 1);
            
            for point_indx = 1:size(p_C, 2)
                if (any(isinf(u_v_C(:, point_indx))))
                    u_v_C(:, point_indx) = NaN;
                    status = 2;
                end
                
                if(u_v_C(1, point_indx) > this.im_cols_ || u_v_C(1, point_indx)  < 0 || ...
                        u_v_C(2, point_indx) > this.im_rows_ || u_v_C(2, point_indx) < 0)
                    status = 2;
                    u_v_C(:, point_indx) = NaN;
                end
            end
            u_v_C = round(u_v_C + ...
                this.pixel_measurement_noise_amplitude_*...
                (rand(size(u_v_C)))- 0.5);
        end
        
        function setTimeOfCapture(this, time_stamp)
            this.time_of_capture_ = time_stamp;
        end
        
        function time_stamp = getTimeOfCapture(this)
            time_stamp = this.time_of_capture_ + ...
                this.time_of_capture_measurement_noise_ * (rand(1, 1)-0.5);
        end
        
        function pose = getPose(this)
            pose = this.camera_pose_;
        end
        
        function sendCalibration(this, tcp_port, endianness)
            R_CG = quat2rotm(this.camera_pose_(4:7, 1)');
            p_C = this.camera_pose_(1:3, 1);
            T_CG_transpose = [R_CG, -p_C]';
            if(endianness == Communication.kBigEndian)
                %Send internal calibration first
                write(tcp_port,...
                    swapbytes(single([this.f_x_, 0, this.c_x_, this.f_y_, this.c_y_])));
                % now Send external calibration
                write(tcp_port, swapbytes(single(T_CG_transpose(:))));
            else
                %Send internal calibration first
                write(tcp_port,...
                    single([this.f_x_, 0, this.c_x_, this.f_y_, this.c_y_]));
                % now Send external calibration
                write(tcp_port, single(T_CG_transpose(:)));
            end
        end
    end
end