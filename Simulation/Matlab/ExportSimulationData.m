
% This file sends simulation data to any external estimator
clc
clear all
close all

size = struct('x_', 1, 'y_', 1, 'z_', 1);
num_cameras = 3;
cameras = cell(num_cameras, 1);
num_markers = 1; % the c program currently can not handle more than 1 marker
markers = cell(num_markers, 1);

f_x = 320;
f_y = 320;

im_rows = 480;
im_cols = 640;

c_x = 240;
c_y = 320;

% set cameras in the corners of the workspace
origin = zeros(3, 1);
camera_positions = [-0.5,  0.5, 0.5;...
                    -0.5, -0.5, 0.5;...
                    -0.5,  0.5, 0.5];
                
for cam_index = 1:num_cameras
    camera_pose(1:3, 1) = camera_positions(:, cam_index);
    camera_line_of_sight = (origin - camera_pose(1:3, 1));
    camera_line_of_sight = camera_line_of_sight / norm(camera_line_of_sight);
    camera_pose(4:7, 1) = utility.zVector2Quat(camera_line_of_sight);
    cameras{cam_index} = Camera(f_x, f_y, im_rows, im_cols, c_x, c_y, camera_pose);
end

% Establish one connection for each camera
t = cell(3, 0);
for cam_index = 1:num_cameras
    t{cam_index} = tcpclient('127.0.0.1', 3000);
end

% write camera calibration for each
for cam_index = 1:num_cameras
    cameras{cam_index}.sendCalibration(t{cam_index}, Communication.kBigEndian);
end

marker_postion = [0.0; 0.0; 0.0];
marker_velocity = [0.01; 0.02; 0.01];
marker_acc = [0; 0; 0];
for marker_indx = 1:num_markers
    markers{marker_indx} = Marker(marker_postion, marker_velocity, marker_acc, MarkerBehaviour.KBoundedRandomMotion);
end

motion_capture_arena = MotionCaptureArena(size, cameras, markers);

% Main loop to simulate and post image data
dt = 0.01;
current_time_stamp = 0;

fid = fopen('groundTruthPosition.txt','w');
for numtime_steps = 1:2000 % change this to run the simulation longer
    tic
    motion_capture_arena.runForOneTimeStep(dt);
    current_time_stamp = current_time_stamp + dt;
    [u_v_C, projecton_errors] = ...
        motion_capture_arena.getMarkerProjectionsInImages();
    % write groundtruth to file
    fprintf(fid, motion_capture_arena.dispTrueMarkerStates());
    motion_capture_arena.updateTrueMarkerState(true);
    
    % Send the marker position and capture time
    for cam_index = 1:num_cameras
        write(t{cam_index},...
            swapbytes(int64(cameras{cam_index}.getTimeOfCapture()*1e3)));
        write(t{cam_index},...
            swapbytes(single(u_v_C(:, 1, cam_index))));
    end
%     toc
pause(0.001);
end

fclose(fid);
clear t


