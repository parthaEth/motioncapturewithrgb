% This file is the file that is the entry point of the simulation
clc
clear all
close all

size = struct('x_', 1, 'y_', 1, 'z_', 1);
num_cameras = 3;
cameras = cell(num_cameras, 1);
num_markers = 1;
markers = cell(num_markers, 1);

f_x = 320;
f_y = 320;

im_rows = 480;
im_cols = 640;

c_x = 240;
c_y = 320;

% set cameras in the corners of the workspace
origin = zeros(3, 1);
camera_positions = [-0.5,  0.5, 0.5;...
                    -0.5, -0.5, 0.5;...
                    -0.5,  0.5, 0.5];    
for cam_index = 1:num_cameras
    camera_pose(1:3, 1) = camera_positions(:, cam_index);
    camera_line_of_sight = (origin - camera_pose(1:3, 1));
    camera_line_of_sight = camera_line_of_sight / norm(camera_line_of_sight);
    camera_pose(4:7, 1) = utility.zVector2Quat(camera_line_of_sight);
    cameras{cam_index} = Camera(f_x, f_y, im_rows, im_cols, c_x, c_y, camera_pose);
end

marker_postion = [-0.01; -0.01; -0.01];
marker_velocity = [0.05; 0.075; 0.092];
marker_acc = [0; 0; 0];
for marker_indx = 1:num_markers
    markers{marker_indx} = Marker(marker_postion, marker_velocity, marker_acc, MarkerBehaviour.KBoundedRandomMotion);
end

motion_capture_arena = MotionCaptureArena(size, cameras, markers);

% Instentiate triangulation algorithm. 
tracked_markers = cell(num_markers, 1);
for marker_indx = 1:num_markers
    tracked_markers{marker_indx} = Marker();
end
triangulation = TriangulateMarker(motion_capture_arena, tracked_markers);

% Main loop to simulate and estimate the marker poses
dt = 0.01;
current_time_stamp = 0;

for numtime_steps = 1:2000 % change this to run the simulation longer
    tic
    motion_capture_arena.runForOneTimeStep(dt);
    current_time_stamp = current_time_stamp + dt;
    triangulation.solveMarkerStates(current_time_stamp);
    motion_capture_arena.dispTrueMarkerStates();
    motion_capture_arena.updateTrueMarkerState(true);
    triangulation.dispMarkerStates();
    triangulation.updateTrackedMarkerState(true);
    toc
end

