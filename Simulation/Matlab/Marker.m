classdef Marker < handle
    properties
        % Ground truth position of the marker. [x; y; z]
        position_ = [0; 0; 0];
        
        % Ground truth velocity of the marker. [v_x, v_y, v_z]
        velocity_ = [0; 0; 0];
        
        % Ground truth acceleration of the marker. [a_x, a_y, a_z]
        acc_ = [0; 0; 0];
        
        % By default markers' acceleration do not change
        marker_behaviour_ = MarkerBehaviour.KNoChange;
        acc_amplitude_ = 1; % m/s^2; used only when the marker does random walk
    end
    
    methods
        
        function this = Marker(position, velocity, acc, marker_behaviour)
            if(nargin < 4)
                return;
            end
            
            this.position_ = position;
            this.velocity_ = velocity;
            this.acc_ = acc;
            this.marker_behaviour_ = marker_behaviour;
        end
        
        function randomWalker(this)
            this.acc_ = (rand(3, 1) - 0.5) * this.acc_amplitude_;
        end
        
        function boundedRandomWalker(this)
            this.acc_ = (rand(3, 1) - 0.5) * this.acc_amplitude_;
            for i=1:3
                if this.position_(i) > 0.4 || this.position_(i) < -0.4
                    this.velocity_(i) = -this.velocity_(i); % friction-less reflection
                end
            end
        end
        
        function simulateForDt(this, dt)
            this.position_ = this.position_ + this.velocity_ * dt;
            this.velocity_ = this.velocity_ + this.acc_ * dt;
            if this.marker_behaviour_ == MarkerBehaviour.KRandomMotion
                this.randomWalker();
            elseif this.marker_behaviour_ == MarkerBehaviour.KBoundedRandomMotion
                this.boundedRandomWalker();
            end
        end
        
        function setAcc(this, acc)
            this.acc_ = acc;
        end
        
        function p_G = getPosition(this)
            p_G = this.position_;
        end
        
        function v_G = getVelocity(this)
            v_G = this.velocity_;
        end
        
        function a_G = getAcc(this)
            a_G = this.acc_;
        end
        
        function setVelocity(this, vel)
            this.velocity_ = vel;
        end
        
        function setPosition(this, pos)
            this.position_ = pos;
        end
    end
end