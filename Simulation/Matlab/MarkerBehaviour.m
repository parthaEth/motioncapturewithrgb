classdef MarkerBehaviour
   enumeration
      KNoChange, KRandomMotion, KBoundedRandomMotion
   end
end