% Simulate the Arena and the camera structure
classdef MotionCaptureArena < handle
    
    properties
        size_ = struct('x_', 1, 'y_', 1, 'z_', 1);
        num_cameras_ = 3;
        cameras_ = cell(3, 1);
        num_markers_ = 1;
        markers_ = cell(1, 1);
        current_simulation_time_ = 0;
        figure_handle_ = 0;
        marker_handles_ = zeros(1, 1);
        % A camera is represented by three quivers
        camera_handles_ = zeros(3, 3);
        
        % x is red, y is green, zis blue. The camera looks in the z
        % direction.
        axis_colors_ = {'r', 'g', 'b'}
    end
    
    methods
        function this = MotionCaptureArena(size, cameras, markers)
            if(nargin < 3)
                for cam_index = 1:this.num_cameras_
                    this.cameras_{cam_index} = Camera();
                end
                for marker_indx = 1:this.num_markers_
                    this.markers_{marker_indx} = Marker();
                end
                this.initPlot();
                return;
            end
            this.size_.x_ = size.x_;
            this.size_.y_ = size.y_;
            this.size_.z_ = size.z_;
            if(length(cameras) < 3)
                error('Need more than 2 cameras atleast!');
            end
            this.cameras_ = cameras;
            this.num_cameras_ = length(cameras);
            
            this.num_markers_ = length(markers);
            this.markers_ = markers;
            this.initPlot();
        end
        
        function initPlot(this)
            scale = 10;
            
            this.figure_handle_ = figure;
            % plot workspace
            this.drawCube();
            hold on
            
            % Plot Cameras
            for cam_index = 1:this.num_cameras_
                camera_pose = this.cameras_{cam_index}.getPose();
                % Since we represent the camera orientation with active
                % quaternions, i.e. if we want to transform a point in
                % global co-ordinate to camera frame we need to multiply
                % the point co-ordinates with the rotation matrix of the
                % quaternion, the rotation matrix contain the unit vectors
                % of global frame in local axis. So inorder to plot the
                % camera itself we need to take a trnspose as is done in
                % the next line.
                camera_orientation = quat2rotm(camera_pose(4:7, 1)')' ./ scale;
                camera_position = camera_pose(1:3);
                for i=1:3
                    this.camera_handles_(cam_index, i) = ...
                        quiver3(camera_position(1),...
                        camera_position(2),...
                        camera_position(3),...
                        camera_orientation(1, i),...
                        camera_orientation(2, i),...
                        camera_orientation(3, i), this.axis_colors_{i});
                end
                drawnow;
            end
            
            % Plot markers
            for marker_indx = 1:this.num_markers_
                marker_position = this.markers_{marker_indx}.getPosition();
                this.marker_handles_(marker_indx) = ...
                    plot3(marker_position(1),...
                    marker_position(2),...
                    marker_position(3),'*');
            end
        end
        
        function addMarkers(this)
        end
        
        function addCameras(this)
        end
        
        function removeCameras(this, camera_name)
        end
        
        function removeMarkers(this, marker_name)
        end
        
        function runForOneTimeStep(this, dt)
            for marker_indx = 1:this.num_markers_
                this.markers_{marker_indx}.simulateForDt(dt);
            end
            this.current_simulation_time_ = ...
                this.current_simulation_time_ + dt;
            timedelays = sort((rand(this.num_cameras_, 1) - 0.5));
            for current_cam = 1:this.num_cameras_
                % To simulate random capture by different cameras.
                capture_time = ...
                    this.current_simulation_time_ + timedelays(current_cam) * dt;
                this.cameras_{current_cam}.setTimeOfCapture(capture_time);
            end
        end
        
        function [u_v_C, projecton_errors] = getMarkerProjectionsInImages(this)
            projecton_errors = zeros(this.num_markers_);
            p_G = zeros(3, this.num_markers_);
            p_G_when_captured = p_G;
            for marker_indx = 1:this.num_markers_
                p_G(:, marker_indx) = ...
                    this.markers_{marker_indx}.getPosition();
            end
            
            u_v_C = zeros(2, size(p_G, 2), this.num_cameras_);
            
            for current_cam = 1:this.num_cameras_
                capture_time = this.cameras_{current_cam}.getTimeOfCapture();
                time_lapse = capture_time - this.current_simulation_time_;
                for marker_indx = 1:this.num_markers_
                    % Assuming constant velocity model during two
                    % simulation steps.
                    p_G_when_captured(:, marker_indx) = p_G(:, marker_indx) + ...
                        this.markers_{marker_indx}.getVelocity() * time_lapse;
                end
                [u_v_C(:, :, current_cam), projecton_errors(current_cam)] =...
                    getProjectionInImages(this, p_G_when_captured, current_cam);
            end
        end
        
        function [u_v_C, error_code] = getProjectionInImages(this, p_G, camera_index)
            if(size(p_G, 1) ~= 3)
                error(['The points must be located in global frame with'...
                    , 'each column representing one point.']);
            end            
            [u_v_C, error_code] = ...
                this.cameras_{camera_index}.project3d(p_G);
        end
        
        function cameras = getCameras(this)
            cameras = this.cameras_;
        end
        
        function updateTrueMarkerState(this, plot_trail)
            % This function is called optionally after each simulation
            % step. The plot is then updated. Usually the marker positions
            % change.
            persistent previous_marker_position;
            for i = 1:this.num_markers_
                current_marker_position = this.markers_{i}.getPosition();
                set(this.marker_handles_,'XData',current_marker_position(1));
                set(this.marker_handles_,'YData',current_marker_position(2));
                set(this.marker_handles_,'ZData',current_marker_position(3));
            end
            if nargin==2 && ~isempty(previous_marker_position)
                if plot_trail
                    line([previous_marker_position(1), current_marker_position(1)],...
                         [previous_marker_position(2), current_marker_position(2)],...
                         [previous_marker_position(3), current_marker_position(3)]);
                end
            end
            drawnow;
            previous_marker_position = current_marker_position;
        end
        
        function drawCube (this, origin)
            if(nargin < 2)
                origin = zeros(3, 1);
            end
            x=([0 1 1 0 0 0;1 1 0 0 1 1;1 1 0 0 1 1;0 1 1 0 0 0]-0.5)*this.size_.x_+origin(1);
            y=([0 0 1 1 0 0;0 1 1 0 0 0;0 1 1 0 1 1;0 0 1 1 1 1]-0.5)*this.size_.y_+origin(2);
            z=([0 0 0 0 0 1;0 0 0 0 0 1;1 1 1 1 0 1;1 1 1 1 0 1]-0.5)*this.size_.z_+origin(3);
            for i=1:6
                h=patch(x(:,i),y(:,i),z(:,i),'w','FaceColor','none');
                set(h,'edgecolor','k')
            end
            xlabel('X \rightarrow');
            ylabel('Y \rightarrow');
            zlabel('Z \rightarrow');
        end
        
        function line = dispTrueMarkerStates(this)
            line = '';
            for i = 1:this.num_markers_
               line = [line, num2str(this.markers_{i}.getPosition()'), ' '];
            end
            disp(line);
            line = [line, '\n'];
        end
    end
end