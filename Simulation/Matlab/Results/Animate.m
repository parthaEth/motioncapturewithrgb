clc
clear all
close all

true_pos = load('Data/groundTruthPosition.txt');
estimated_pos = load('Data/states.csv');
pose_cov = load('Data/states_cov.csv');

for i = 1:2000%size(true_pos, 1)
    plot3(true_pos(1:i, 1), true_pos(1:i, 2), true_pos(1:i, 3), 'g')
    hold on
    plot3(true_pos(i, 1), true_pos(i, 2), true_pos(i, 3), 'g*')
    
    estimated_pos_idx = i*3;
    plot3(estimated_pos(1:estimated_pos_idx, 1), estimated_pos(1:estimated_pos_idx, 2), estimated_pos(1:estimated_pos_idx, 3), 'r')
    plot3(estimated_pos(estimated_pos_idx, 1), estimated_pos(estimated_pos_idx, 2), estimated_pos(estimated_pos_idx, 3), 'r*')
    cov_row = (estimated_pos_idx-1)*6 + 1;
%     plot_gaussian_ellipsoid([estimated_pos(estimated_pos_idx, 1), estimated_pos(estimated_pos_idx, 2), estimated_pos(estimated_pos_idx, 3)], ...
%                             pose_cov(cov_row:cov_row+2,1:3))
    hold off
    pause(0.01)
end
