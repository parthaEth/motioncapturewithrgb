classdef TriangulateMarker < handle
    % Computes 3d marker positions and their velocity given image time
    % stamps and detected marker locations.
    properties
        num_cameras_ = 0;
        cameras_ = cell(0, 1); % set from solveMarkerStates
        num_markers_ = 0;
        markers_ = cell(0, 1);
        % used as seed to the optimization each time the method triangulate
        % is called. First three are positions while next are velocities.
        marker_state_ = zeros(6, 0);
        current_time_stamp_ = 0; % set from solveMarkerStates
        previous_time_stamp_ = 0;
        motion_capture_arena_;
        projection_errors_;
        marker_handles_ = zeros(1, 1);
    end
    
    methods
        function this = TriangulateMarker(motion_capture_arena, markers)
            if(nargin < 2)
                for marker_indx = 1:this.num_markers_
                    this.markers_{marker_indx} = Marker();
                end
            end
            this.motion_capture_arena_ = motion_capture_arena;
            this.cameras_ = this.motion_capture_arena_.getCameras();
            this.markers_ = markers;
            this.num_markers_ = length(markers);
            this.marker_state_ = zeros(6, this.num_markers_);
            this.num_cameras_ = length(this.cameras_);
            this.initPlot();
        end
        
        function initPlot(this)
            for marker_indx = 1:this.num_markers_
                marker_position = this.markers_{marker_indx}.getPosition();
                this.marker_handles_(marker_indx) = ...
                    plot3(marker_position(1),...
                    marker_position(2),...
                    marker_position(3),'r*');
            end
        end
        
        function updateTrackedMarkerState(this, plot_trail)
            persistent previous_tracked_marker_position;
            for i = 1:this.num_markers_
                current_marker_position = this.markers_{i}.getPosition();
                set(this.marker_handles_,'XData',current_marker_position(1));
                set(this.marker_handles_,'YData',current_marker_position(2));
                set(this.marker_handles_,'ZData',current_marker_position(3));
            end
            if nargin==2 && ~isempty(previous_tracked_marker_position)
                if plot_trail
                    line([previous_tracked_marker_position(1), current_marker_position(1)],...
                         [previous_tracked_marker_position(2), current_marker_position(2)],...
                         [previous_tracked_marker_position(3), current_marker_position(3)], 'color', [1, 0, 0]);
                end
            end
            drawnow;
            previous_tracked_marker_position = current_marker_position;
        end
        
        function addMarker(this)
        end
        
        function addCamera(this)
        end
        
        % fix
        function solveMarkerStates(this, current_time_stamp)
            this.current_time_stamp_ = current_time_stamp;
            
            % Note that u_v_C may contain NaN-s if projection_errors_ have
            % anything other than zeros. we let these to go into lsqcurvefit
            % but compute their residals to be seros so as to not alter
            % the optimzation result.
            [u_v_C, this.projection_errors_] = this.motion_capture_arena_.getMarkerProjectionsInImages();
            
            % Number of residuals are 2*num_cameras*num_markers, as each
            % camera produces two measurements.
            this.marker_state_ = ...
                lsqcurvefit(@this.computeResiduals, this.marker_state_, ...
                u_v_C, zeros(this.num_cameras_ * this.num_markers_ * 2, 1));
            this.previous_time_stamp_ = current_time_stamp;
            for marker_indx = 1:this.num_markers_
                this.markers_{marker_indx}.setPosition(this.marker_state_(1:3, marker_indx));
                this.markers_{marker_indx}.setVelocity(this.marker_state_(4:6, marker_indx));
            end
        end
        
        function dispMarkerStates(this)
            line = '';
            for i = 1:this.num_markers_
                line = [line, ' [', num2str(this.markers_{i}.getPosition()'), '] '];
            end
            disp(line);
        end
        
    end
    
    methods(Access = private)
        function residuals = computeResiduals(this, marker_state, detected_projection)
            % Number of residuals are 2*num_cameras*num_markers, as each
            % camera produces two measurements.
            residuals = zeros(size(detected_projection, 3) *...
                              size(detected_projection, 2) *...
                              size(detected_projection, 1), 1);
            count = 1;
            expected_marker_position = this.computeMarkerPositionForEachCamera(marker_state);
            for marker_idx = 1:this.num_markers_
                for cam_index = 1:this.num_cameras_
                    % This marker is not visible in this camera so no
                    % constraint is posed on its state.
                    if(this.projection_errors_(cam_index) ~= 0 && any(isnan(detected_projection(:, marker_idx, cam_index))))
                        residuals(count) = 0;
                        continue;
                    end
                    
                    expected_projection = ...
                        this.cameras_{cam_index}.project3d(expected_marker_position(:, marker_idx, cam_index));
                    residuals(count) = ...
                        norm(detected_projection(:, marker_idx, cam_index) - expected_projection);
                    count = count + 1;
                end
            end
        end
        
        function p_G_expect = computeMarkerPositionForEachCamera(this, marker_state)
            % marker_state : = each cloumn corresponds to one marker. First
            % three entries are positions and next three are velocity.
            % Each column represents one marker. % Each layer represents
            % one camera. Dimension of p_G_expect is
            % 3 X num_markers X num_cam
            p_G_expect = zeros(3, this.num_markers_, this.num_cameras_);
            for marker_idx = 1:this.num_markers_
                for cam_index = 1:this.num_cameras_
                    time_since_last_triangulation = ...
                        this.cameras_{cam_index}.getTimeOfCapture() - this.previous_time_stamp_;
                    
                    % Constant velocity model is being enforced here.
                    % We may change to constant acceleration later
                    % here.
                    p_G_expect(:, marker_idx, cam_index) = ...
                        marker_state(1:3, marker_idx) + ...
                        time_since_last_triangulation * marker_state(4:6, marker_idx);
                end
            end
        end
    end
end