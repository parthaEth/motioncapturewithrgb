% Unit tests
clc
clear all
close all

% Simple test camera
camera = Camera();

p_G = [0; 0; 1];
[u_v_calculated, ~] = camera.project3d(p_G);

if(u_v_calculated ~= [240; 320])
    error('Test failed');
end

% Simple test marker
velocity = [0.6; 0.7; 0.5];
marker = Marker([0; 0; 0], velocity, [0; 0; 0]);
dt = 0.1;

expected_position = zeros(3, 1);
for time_steps = 1:5
    expected_position = expected_position + velocity * dt;
    marker.simulateForDt(dt);
    if(expected_position ~= marker.getPosition())
        error('Test failed');
    end
end

marker.setAcc([1; 2; 3])

body_rot = quat2rotm(utility.zVector2Quat([1, 0, 0]))';
if(norm(body_rot(:, 3)-[1; 0; 0]) > 1e-4)
    error('Utility quaternion functions Test failed');
end