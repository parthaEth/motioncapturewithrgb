classdef utility
    %UTILITY class for utility functions. Every method and property in here
    % will of type static.
    
    properties
    end
    
    methods(Static)
        function quat = zVector2Quat(z_vec)
            % x and y is set randomly. This means that if this function is
            % used for setting camera orientation then the camera will be
            % rotated randomly around the line of sight. For all practical
            % puepose this should not matter.
            if(size(z_vec, 1) ~= 3)
                z_vec = z_vec';
            end
            if(norm(z_vec) > 1 + 1e-4)
                error('Provided direction vector must be of unit length.');
            end
            random_unit_vec = rand(size(z_vec)) - 0.5;
            random_unit_vec = random_unit_vec/norm(random_unit_vec);
            x_vec = cross(random_unit_vec, z_vec);
            x_vec = x_vec/norm(x_vec);
            y_vec = -cross(x_vec, z_vec);
            quat = rotm2quat([x_vec, y_vec, z_vec]');
        end
        
        function plotActiveQuat(quat, figure_id)
            % Since we are dealing with with active
            % quaternions, i.e. if we want to transform a point in
            % global co-ordinate to local frame we need to multiply
            % the point co-ordinates with the rotation matrix of the
            % quaternion, the rotation matrix contain the unit vectors
            % of global frame in local axis. So inorder to plot the
            % local-frame itself we need to take a trnspose as is done
            % below.
            if(norm(quat) > 1 + 1e-4)
                error('Provided orientation quaternion must be of unit length.');
            end
            if(nargin > 1)
                figure(figure_id);
            else
                figure();
            end
            
            R = quat2rotm(quat)';
            quiver3(0,0,0,R(1,1),R(2,1),R(3,1),'r')
            hold on
            quiver3(0,0,0,R(1,2),R(2,2),R(3,2), 'g')
            quiver3(0,0,0,R(1,3),R(2,3),R(3,3), 'b')
            xlabel('X \rightarrow');
            ylabel('Y \rightarrow');
            zlabel('Z \rightarrow');
        end
    end
    
end

