rgb = imread('D:/AsyMoCapData/DetectBall.png');
grey_img = rgb2gray(rgb);
imshow(grey_img)
level = graythresh(grey_img);
bw = im2bw(grey_img, level*2.2);

se = strel('disk',20);
bw = imclose(bw,se);
bw = imfill(bw,'holes');
bw = bwareaopen(bw, 100);
imshow(bw);

[centers, radii] = imfindcircles(bw,[10 20],'ObjectPolarity','bright', ...
    'Sensitivity',0.8);
imshow(bw);

blobAnalyser = vision.BlobAnalysis('BoundingBoxOutputPort', true, ...
            'AreaOutputPort', true, 'CentroidOutputPort', true, ...
            'MinimumBlobArea', 40);
[~, ~, bboxes] = blobAnalyser.step(bw);
h = viscircles(centers,radii);

hold on

rectangle('Position', bboxes(1, :), 'EdgeColor', 'r', 'LineWidth', 3)
rectangle('Position', bboxes(2, :), 'EdgeColor', 'r', 'LineWidth', 3)
rectangle('Position', bboxes(3, :), 'EdgeColor', 'r', 'LineWidth', 3)
rectangle('Position', bboxes(4, :), 'EdgeColor', 'r', 'LineWidth', 3)