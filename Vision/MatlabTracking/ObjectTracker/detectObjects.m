function [centroids, bboxes, mask] = detectObjects(frame, obj, tracks)

num_tracks = length(tracks)

% % Detect foreground.
% mask = obj.detector.step(frame);
% 
% % Apply morphological operations to remove noise and fill in holes.
% mask = imopen(mask, strel('rectangle', [3,3]));
% mask = imclose(mask, strel('rectangle', [15, 15]));
% mask = imfill(mask, 'holes');
[image_height, image_width, ~] = size(frame);
mask = true(image_height, image_width);

Imge_threshold = 0.85; % may be make it adaptive
centers_acc = [];
radis = [];

if num_tracks == 0
    
    % Perform blob analysis to find connected components.
    [~, ~, bboxes] = obj.blobAnalyser.step(mask);

    % Search for circle in whole image
    for i=1:size(bboxes, 1)
        imge_patch = frame(bboxes(i, 2):bboxes(i, 2) + bboxes(i, 4) - 1,...
            bboxes(i, 1):bboxes(i, 1) + bboxes(i, 3) - 1, :);
        imge_patch = rgb2gray(imge_patch);
        imge_patch = im2bw(imge_patch, Imge_threshold);
        
        %Need to addapt for relative values
        se = strel('disk',20);
        imge_patch = imclose(imge_patch, se);
        imge_patch = imfill(imge_patch,'holes');
        imge_patch = bwareaopen(imge_patch, 100);
        
        [centers, radii] = imfindcircles(imge_patch,[10 20],'ObjectPolarity','bright', ...
            'Sensitivity',0.8);
        centers_acc = [centers_acc; int32(centers)];
        radis = [radis; int32(radii)];
    end
    
else
    half_inflation_scale = 1;
    for i=1:1%length(tracks)
        inflated_bbox(1) = max(tracks(i).bbox(1) - ...
            half_inflation_scale*tracks(i).bbox(3), 1);
        inflated_bbox(2) = max(tracks(i).bbox(2) - ...
            half_inflation_scale*tracks(i).bbox(4), 1);
        
        inflated_bbox(3) = (2*half_inflation_scale + 1) * ...
            tracks(i).bbox(3);
        
        if inflated_bbox(1) + inflated_bbox(3) > image_width
            inflated_bbox(3) = image_width - inflated_bbox(1);
        end
        
        inflated_bbox(4) = (2*half_inflation_scale + 1) * ...
            tracks(i).bbox(4);
        
        if inflated_bbox(4) + inflated_bbox(2) > image_height
            inflated_bbox(4) = image_height - inflated_bbox(2);
        end
        
        part_mask = mask(inflated_bbox(2):inflated_bbox(2) + inflated_bbox(4) - 1, ...
            inflated_bbox(1):inflated_bbox(1) + inflated_bbox(3) - 1);
        part_frame = frame(inflated_bbox(2):inflated_bbox(2) + inflated_bbox(4) - 1, ...
            inflated_bbox(1):inflated_bbox(1) + inflated_bbox(3) - 1, :);
        
        part_frame = rgb2gray(part_frame);
        
        part_frame = part_frame .* part_mask;
        
        [centers, radii] = imfindcircles(part_frame,[10 20],'ObjectPolarity','bright', ...
            'Sensitivity',0.85);
        
        if ~isempty(centers)
            centers_acc = [centers_acc; ...
                int32(centers) + repmat(inflated_bbox(1:2), size(centers, 1), 1)];
            radis = [radis; int32(radii)];
        end
    end
    
end
centroids = int32(centers_acc);
bboxes = [centers_acc-repmat(radis, 1, 2), 2*repmat(radis, 1, 2)];

end