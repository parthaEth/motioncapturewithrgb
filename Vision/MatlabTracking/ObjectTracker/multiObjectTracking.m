function multiObjectTracking()
% Create System objects used for reading video, detecting moving objects,
% and displaying the results.
obj = setupSystemObjects();

tracks = initializeTracks(); % Create an empty array of tracks.

% Detect moving objects, and track them across video frames.
% while ~isDone(obj.reader)
for i=500:2000
    frame = readFrame(obj);
    [centroids, bboxes, mask] = detectObjects(frame, obj, tracks);
    tracks = predictNewLocationsOfTracks(tracks);
    [assignments, unassignedTracks, unassignedDetections] = ...
        detectionToTrackAssignment(tracks, centroids);

    tracks = updateAssignedTracks(tracks, centroids, bboxes, assignments);
    tracks = updateUnassignedTracks(tracks, unassignedTracks);
    tracks = deleteLostTracks(tracks);
    tracks = createNewTracks(tracks, centroids, bboxes, unassignedDetections);

    displayTrackingResults(obj, frame, mask, tracks);
end


