function frame = readFrame(obj)
    frame = obj.reader.step();
    frame = imresize(frame, 0.5);
end