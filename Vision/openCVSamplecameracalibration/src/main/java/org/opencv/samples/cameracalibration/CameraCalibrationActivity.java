// This sample is based on "Camera calibration With OpenCV" tutorial:
// http://docs.opencv.org/doc/tutorials/calib3d/camera_calibration/camera_calibration.html
//
// It uses standard OpenCV asymmetric circles grid pattern 11x4:
// https://github.com/Itseez/opencv/blob/2.4/doc/acircles_pattern.png.
// The results are the camera matrix and 5 distortion coefficients.
//
// Tap on highlighted pattern to capture pattern corners for calibration.
// Move pattern along the whole screen and capture data.
//
// When you've captured necessary amount of pattern corners (usually ~20 are enough),
// press "Calibrate" button for performing camera calibration.

package org.opencv.samples.cameracalibration;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;

public class CameraCalibrationActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;
    private CameraCalibrator mCalibrator;
    private OnCameraFrameRender mOnCameraFrameRender;
    private int mWidth;
    private int mHeight;

    private boolean              mIsColorSelected = false;
    private Mat                  mRgba;
    private Scalar               mBlobColorRgba;
    private Scalar               mBlobColorHsv;
    private ColorBlobDetector    mDetector;
    private Mat                  mSpectrum;
    private Size                 SPECTRUM_SIZE;
    private Scalar               CONTOUR_COLOR;
    private long time ;
    private float[] centre = new float[2];

    SocketConnector socketConnector_ ;
    Socket orientationConsumer_;
    OutputStream outToOrientationConsumer_;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
            case LoaderCallbackInterface.SUCCESS:
            {
                Log.i(TAG, "OpenCV loaded successfully");
                mOpenCvCameraView.enableView();
                mOpenCvCameraView.setOnTouchListener(CameraCalibrationActivity.this);
            } break;
            default:
            {
                super.onManagerConnected(status);
            } break;
            }
        }
    };

    public CameraCalibrationActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.camera_calibration_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_calibration_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        socketConnector_ = new SocketConnector();
        socketConnector_.start();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.calibration, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.preview_mode).setEnabled(true);
        if (!mCalibrator.isCalibrated())
            menu.findItem(R.id.preview_mode).setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.calibration:
            mOnCameraFrameRender =
                new OnCameraFrameRender(new CalibrationFrameRender(mCalibrator));
            item.setChecked(true);
            return true;
        case R.id.undistortion:
            mOnCameraFrameRender =
                new OnCameraFrameRender(new UndistortionFrameRender(mCalibrator));
            item.setChecked(true);
            return true;
        case R.id.comparison:
            mOnCameraFrameRender =
                new OnCameraFrameRender(new ComparisonFrameRender(mCalibrator, mWidth, mHeight, getResources()));
            item.setChecked(true);
            return true;
        case R.id.calibrate:
            final Resources res = getResources();
            if (mCalibrator.getCornersBufferSize() < 2) {
                (Toast.makeText(this, res.getString(R.string.more_samples), Toast.LENGTH_SHORT)).show();
                return true;
            }

            mOnCameraFrameRender = new OnCameraFrameRender(new PreviewFrameRender());
            new AsyncTask<Void, Void, Void>() {
                private ProgressDialog calibrationProgress;

                @Override
                protected void onPreExecute() {
                    calibrationProgress = new ProgressDialog(CameraCalibrationActivity.this);
                    calibrationProgress.setTitle(res.getString(R.string.calibrating));
                    calibrationProgress.setMessage(res.getString(R.string.please_wait));
                    calibrationProgress.setCancelable(false);
                    calibrationProgress.setIndeterminate(true);
                    calibrationProgress.show();
                }

                @Override
                protected Void doInBackground(Void... arg0) {
                    mCalibrator.calibrate();
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    calibrationProgress.dismiss();
                    mCalibrator.clearCorners();
                    mOnCameraFrameRender = new OnCameraFrameRender(new CalibrationFrameRender(mCalibrator));
                    String resultMessage = (mCalibrator.isCalibrated()) ?
                            res.getString(R.string.calibration_successful)  + " " + mCalibrator.getAvgReprojectionError() :
                            res.getString(R.string.calibration_unsuccessful);
                    (Toast.makeText(CameraCalibrationActivity.this, resultMessage, Toast.LENGTH_SHORT)).show();

                    if (mCalibrator.isCalibrated()) {
                        CalibrationResult.save(CameraCalibrationActivity.this,
                                mCalibrator.getCameraMatrix(), mCalibrator.getDistortionCoefficients());

                        double[] cameraMatrixArray = new double[9];
                        double[] extCameraMatrixArray = new double[12];
                        float[] camArraysend = new float[5];
                        float[] extCamArraysend = new float[12];
                        mCalibrator.getCameraMatrix().get(0,  0, cameraMatrixArray);
                        int k = 0;
                        for (int i = 0; i < 2; i++) {
                            for (int j = 0; j < 3; j++) {
                                Integer id = i * 3 + j;
                                if (id != 3 ) {
                                    camArraysend[k++] = (float) cameraMatrixArray[id];
                                }
                            }
                        }

                        mCalibrator.getExtCameraMatrix().get(0,  0, extCameraMatrixArray);


                        while(true) {
                            if (orientationConsumer_ != null) {
                                try {
                                    outToOrientationConsumer_ = orientationConsumer_.getOutputStream();
                                } catch (IOException e) {
                                    System.out.println("Can't establish connection with the orientation consumer");
                                    System.exit(0);
                                }

                                ByteBuffer serialized = ByteBuffer.allocate(4*(camArraysend.length + extCameraMatrixArray.length));
                                for (int i = 0; i < camArraysend.length + extCamArraysend.length ; ++i) {
                                    if (i<camArraysend.length)
                                         serialized.putFloat(camArraysend[i]);
                                    else
                                         serialized.putFloat((float)extCameraMatrixArray[i-camArraysend.length]);
                                }
                                try {
                                    outToOrientationConsumer_.write(serialized.array());
                                    System.out.println("posted data!");
                                } catch (IOException e) {
                                    System.out.println("Receiver missed data!");
                                }
                                break;
                            } else {
                                orientationConsumer_ = socketConnector_.getConnectedSocket();
                                System.out.println("Connection hasn't established yet");
                            }
                        }

                    }
                }
            }.execute();
            return true;

        case R.id.blobdetect:
            mOnCameraFrameRender = new OnCameraFrameRender(new PreviewFrameRender());
            if (mCalibrator.isCalibrated()) {
                mDetector.blob_detect = true;
                mOnCameraFrameRender = new OnCameraFrameRender(new BlobDetectFrameRender(mDetector));
            }
            else
                mDetector.blob_detect = false;
                return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    public void onCameraViewStarted(int width, int height) {
        if (mWidth != width || mHeight != height) {
            mWidth = width;
            mHeight = height;
            mCalibrator = new CameraCalibrator(mWidth, mHeight);
            if (CalibrationResult.tryLoad(this, mCalibrator.getCameraMatrix(), mCalibrator.getDistortionCoefficients())) {
                mCalibrator.setCalibrated();
            }

            mOnCameraFrameRender = new OnCameraFrameRender(new CalibrationFrameRender(mCalibrator));
        }
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        mBlobColorRgba = new Scalar(255);
        mBlobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(255,0,0,255);
    }

    public void onCameraViewStopped() {mRgba.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        if (!mDetector.isStarted()) {
            return mOnCameraFrameRender.render(inputFrame);
        }
        else
        {
            mRgba = mOnCameraFrameRender.render(inputFrame);
            time = System.currentTimeMillis();
            Log.e(TAG, "Current timestamp: " + time);
            if (mIsColorSelected) {
                mDetector.process(mRgba);
                List<MatOfPoint> contours = mDetector.getContours();
                List<Point> centroid = mDetector.getCentroid();
                Log.e(TAG, "Contours count: " + contours.size());
                Log.e(TAG, "Centroid count: " + centroid.size());
                Iterator<Point> each= centroid.iterator();
                while (each.hasNext()) {
                    Point cen = each.next();
                    Log.i(TAG, "Centroid: X" + cen.x + " Y :" + cen.y );
                    centre[0]= (float)cen.x;
                    centre[1]= (float)cen.y;
                    Imgproc.drawMarker(mRgba,cen,CONTOUR_COLOR);
                }

                Imgproc.drawContours(mRgba,contours,-1,CONTOUR_COLOR);
                Mat colorLabel = mRgba.submat(4, 68, 4, 68);
                colorLabel.setTo(mBlobColorRgba);

                Mat spectrumLabel = mRgba.submat(4, 4 + mSpectrum.rows(), 70, 70 + mSpectrum.cols());
                mSpectrum.copyTo(spectrumLabel);

                if(orientationConsumer_ != null){
                    try{
                        outToOrientationConsumer_ = orientationConsumer_.getOutputStream();
                    }catch (IOException e) {
                        System.out.println("Can't establish connection with the orientation consumer");
                        System.exit(0);
                    }

                    ByteBuffer serialized = ByteBuffer.allocate(4 * centre.length + 8 );
                    //System.out.println("Long size" + Long.SIZE);
                    serialized.putLong(time);
                    for(int i = 0; i < centre.length; ++i){
                        serialized.putFloat(centre[i]);
                    }
                    try{
                        outToOrientationConsumer_.write(serialized.array());
                        System.out.println("posted data!");
                    }catch (IOException e) {
                        System.out.println("Receiver missed data!");
                    }
                }else{
                    orientationConsumer_ = socketConnector_.getConnectedSocket();
                    System.out.println("Connection hasn't established yet");
                }

            }

            return mRgba;
        }


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d(TAG, "onTouch invoked");

            mCalibrator.addCorners();
          //  return false;

        if (mDetector.isStarted()) {

            int cols = mRgba.cols();
            int rows = mRgba.rows();

            int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
            int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;

            int x = (int) event.getX() - xOffset;
            int y = (int) event.getY() - yOffset;

            Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");

            if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;

            Rect touchedRect = new Rect();

            touchedRect.x = (x > 4) ? x - 4 : 0;
            touchedRect.y = (y > 4) ? y - 4 : 0;

            touchedRect.width = (x + 4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
            touchedRect.height = (y + 4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

            Mat touchedRegionRgba = mRgba.submat(touchedRect);

            Mat touchedRegionHsv = new Mat();
            Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

            // Calculate average color of touched region
            mBlobColorHsv = Core.sumElems(touchedRegionHsv);
            int pointCount = touchedRect.width * touchedRect.height;
            for (int i = 0; i < mBlobColorHsv.val.length; i++)
                mBlobColorHsv.val[i] /= pointCount;

            mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);

            Log.i(TAG, "Touched rgba color: (" + mBlobColorRgba.val[0] + ", " + mBlobColorRgba.val[1] +
                    ", " + mBlobColorRgba.val[2] + ", " + mBlobColorRgba.val[3] + ")");

            mDetector.setHsvColor(mBlobColorHsv);

            Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);

            mIsColorSelected = true;

            touchedRegionRgba.release();
            touchedRegionHsv.release();
        }
            return false; // don't need subsequent touch events

    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }

    class SocketConnector extends Thread {
        private Socket s_;
        Double i = new Double(3.0);
        @Override
        public void run() {
            try {
                synchronized(i){
                    s_ = new Socket("192.168.43.162", 3000);
                   //s_ = new Socket("192.168.43.85", 3000); //nachiket
                }
                Log.i(TAG, "Connected");
            }catch(IOException e){
                e.printStackTrace();
            }
        }

        public Socket getConnectedSocket(){
            synchronized(i) {
                return s_;
            }
        }
    }
}
