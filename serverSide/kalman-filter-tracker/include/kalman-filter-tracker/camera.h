/*
 * camera.h
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_CAMERA_H_
#define KALMAN_FILTER_TRACKER_CAMERA_H_

#include <Eigen/Dense>

class Camera{
private:
	Eigen::Matrix3d internal_calibration_;
	Eigen::Matrix3d KR_; //internal matrix times rotation matrix

	int image_rows_;
	int iage_cols_;

	Eigen::Matrix4d external_calibration_;

	void transformToCameraFrame(const Eigen::Matrix3Xd& points,
								Eigen::Matrix3Xd* transformed_points)const;
public:
	Camera(const Eigen::Matrix3d& internal_calibration,
		   const Eigen::Matrix4d& external_calibration,
		   const int image_rows, const int iage_cols);
	void setInternalCalibration(const Eigen::Matrix3d& internal_calibration);
	void setExternalCalibration(const Eigen::Matrix4d& external_calibration);
	bool project3dPoint(const Eigen::Matrix3Xd& points,
			            Eigen::Matrix2Xd* projected_points,
			            Eigen::Matrix<double, 2, 3>* point_derivative) const;
};

#endif /* KALMAN_FILTER_TRACKER_CAMERA_H_ */
