/*
 * kalman-filter-bse.h
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_KALMAN_FILTER_H_
#define KALMAN_FILTER_TRACKER_KALMAN_FILTER_H_

#include <map>
#include <memory>

#include <logger-and-publisher/publish-state-and-cov.h>
#include <server-side-com/client-handler.h>

#include "kalman-filter-tracker/kalman-update.h"
#include "kalman-filter-tracker/propagation-base.h"

class KalmanFilter{
private:
	Eigen::MatrixXd state_;
	Eigen::MatrixXd state_cov_;
	unsigned long long int current_time_in_ms_ = 0;
	std::map<int, std::shared_ptr<KalmanUpdateBase> > update_filter_;
	std::mutex mutex_for_update_filter_map_;
	std::shared_ptr<KalmanPropagtionBase> propaget_filter_;
	LoggerAndPublisher rviz_publisher_;

public:
	KalmanFilter(const unsigned long long int current_time_in_ms,
				 std::shared_ptr<KalmanPropagtionBase>* propaget_filter);

	const Eigen::MatrixXd& getState()const{
		return state_;
	}

	const Eigen::MatrixXd& getStateCov()const{
		return state_cov_;
	}

	// Perform prediction and update
	void estimeteState(const Eigen::MatrixXd& measurements,
					   const unsigned long long int time_stamp_ms);
	bool addNewUpdator(
			const int updator_id,
			std::shared_ptr<KalmanUpdateBase>* updator);
};



#endif /* KALMAN_FILTER_TRACKER_KALMAN_FILTER_H_ */
