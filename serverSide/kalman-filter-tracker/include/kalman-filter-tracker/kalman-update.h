/*
 * kalman-update.h
 *
 *  Created on: Aug 30, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_KALMAN_UPDATE_H_
#define KALMAN_FILTER_TRACKER_KALMAN_UPDATE_H_

#include <Eigen/Dense>

class KalmanUpdateBase{
protected:
	Eigen::MatrixXd* state_;
	Eigen::MatrixXd* state_cov_;

	void setStateAndCovPointer(Eigen::MatrixXd* state,
			                   Eigen::MatrixXd* state_cov){
		CHECK_NOTNULL(state);
		CHECK_NOTNULL(state_cov);
		state_ = state;
		state_cov_ = state_cov;
	}

public:
	KalmanUpdateBase(Eigen::MatrixXd* state, Eigen::MatrixXd* state_cov)
: state_(state), state_cov_(state_cov){}

	virtual ~KalmanUpdateBase(){}
	virtual void updateStateAndCov(const Eigen::MatrixXd measurements) = 0;
	virtual void setStateAndCov(Eigen::MatrixXd* state,
			                    Eigen::MatrixXd* state_cov) = 0;
};



#endif /* KALMAN_FILTER_TRACKER_KALMAN_UPDATE_H_ */
