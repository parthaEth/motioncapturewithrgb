/*
 * propagation-base.h
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_PROPAGATION_BASE_H_
#define KALMAN_FILTER_TRACKER_PROPAGATION_BASE_H_

#include <glog/logging.h>

#include <Eigen/Dense>

class KalmanPropagtionBase{
protected:
	Eigen::MatrixXd* state_;
	Eigen::MatrixXd* state_cov_;

	void setStateAndCovPointer(Eigen::MatrixXd* state,
			                   Eigen::MatrixXd* state_cov){
		CHECK_NOTNULL(state);
		CHECK_NOTNULL(state_cov);
		state_ = state;
		state_cov_ = state_cov;
	}
public:
	KalmanPropagtionBase(Eigen::MatrixXd* state, Eigen::MatrixXd* state_cov)
			: state_(state), state_cov_(state_cov){}

	virtual ~KalmanPropagtionBase(){}

	virtual void propagetStateAndCov(const unsigned long long int to_time_ms) = 0;
	virtual void getStateDerivative(
			  const unsigned long long int to_time_mas,
			  Eigen::MatrixXd* state_derivative) = 0;
	virtual void setStateAndCov(Eigen::MatrixXd* state,
								Eigen::MatrixXd* state_cov) = 0;
};


#endif /* KALMAN_FILTER_TRACKER_PROPAGATION_BASE_H_ */
