/*
 * propaget_marker3d.h
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_PROPAGET_MARKER3D_H_
#define KALMAN_FILTER_TRACKER_PROPAGET_MARKER3D_H_

#include <Eigen/Core>

#include "kalman-filter-tracker/propagation-base.h"

class PropagetMarker3D : public KalmanPropagtionBase{
private:
	const static int kStateDimension = 6;
	const static int kVelocityDimension = 3;
	const static int kPositionDimension = 3;
	// TODO(Partha):Perhaps deploy sec, ms structure
	long long int current_time_ms_;
	Eigen::MatrixXd propagation_noise_;
	Eigen::MatrixXd motion_model_acc_noise_;
	void constVelocityStatePropagation(const unsigned long long int to_time_ms);


public:
	PropagetMarker3D(const double current_time_ms);

	template <typename Derived>
	void setNoiseCov(const Eigen::EigenBase<Derived>& propagation_noise){
		CHECK_EQ(propagation_noise_.rows(), propagation_noise.rows());
		CHECK_EQ(propagation_noise_.cols(), propagation_noise.cols());
		propagation_noise_ = propagation_noise;
	}

	virtual void propagetStateAndCov(
			const unsigned long long int to_time_ms) override;
	virtual void getStateDerivative(
			const unsigned long long int to_time_ms,
			Eigen::MatrixXd* state_derivative) override;
	virtual void setStateAndCov(Eigen::MatrixXd* state,
								Eigen::MatrixXd* state_cov);

	const Eigen::MatrixXd& getState()const{
		return (*state_);
	}

	const Eigen::MatrixXd& getStateCov()const{
		return (*state_cov_);
	}
};

#endif /* PROPAGET_MARKER3D_H_ */
