/*
 * update-marker3d.h
 *
 *  Created on: Aug 30, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_UPDATE_MARKER3D_H_
#define KALMAN_FILTER_TRACKER_UPDATE_MARKER3D_H_

#include "kalman-filter-tracker/camera.h"
#include "kalman-filter-tracker/kalman-update.h"

class UpdateMarker3D : public KalmanUpdateBase{
private:
	const static int kStateDimension = 6;
	const static int kVelocityDimension = 3;
	const static int kPositionDimension = 3;
	const static int kMeasurementDimension = 2;

	Eigen::MatrixXd measurement_noise_;

	const Camera camera_;

public:
	UpdateMarker3D(const Camera& camera);

	virtual ~UpdateMarker3D(){}
	virtual void updateStateAndCov(const Eigen::MatrixXd blob_centre) override;
	virtual void setStateAndCov(Eigen::MatrixXd* state,
								Eigen::MatrixXd* state_cov_) override;


	template <typename Derived>
	void setNoiseCov(const Eigen::EigenBase<Derived>& measurement_noise){
		CHECK_EQ(measurement_noise_.rows(), measurement_noise.rows());
		CHECK_EQ(measurement_noise_.cols(), measurement_noise.cols());
		measurement_noise_ = measurement_noise;
	}
};



#endif /* KALMAN_FILTER_TRACKER_UPDATE_MARKER3D_H_ */
