/*
 * utility.h
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#ifndef KALMAN_FILTER_TRACKER_UTILITY_H_
#define KALMAN_FILTER_TRACKER_UTILITY_H_

#include <random>
#include <cmath>

#include <Eigen/Dense>
#include <glog/logging.h>


namespace Utility{

template <typename DataType, int rows>
void sampleFromMultiVariateGaussian(
		Eigen::Matrix<DataType, rows, rows> covariance,
		Eigen::Matrix<DataType, rows, 1>* sample){
	// TODO(Partha): Correct this function for non diagonal covariance
	VLOG(1) << "Correct this function for non diagonal covariance";
	CHECK_NOTNULL(sample);
	CHECK_EQ(covariance.rows(), covariance.cols());
	sample->resize(covariance.rows(), 1);

	std::random_device rd;
	std::mt19937 gen(rd());

	for(int i= 0; i < covariance.rows(); ++i){
		std::normal_distribution<double> d(0, covariance(i, i));
		(*sample)(i) = d(gen);
	}
}

} /* Utility */


#endif /* KALMAN_FILTER_TRACKER_UTILITY_H_ */
