/*
 * camera.cc
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#include <glog/logging.h>

#include "kalman-filter-tracker/camera.h"

Camera::Camera(const Eigen::Matrix3d& internal_calibration,
		       const Eigen::Matrix4d& external_calibration,
		       const int image_rows, const int iage_cols)
	:internal_calibration_(internal_calibration),
	 external_calibration_(external_calibration),
	 image_rows_(image_rows),
	 iage_cols_(iage_cols){
  CHECK_GT(image_rows, 0);
  CHECK_GT(iage_cols, 0);
  KR_ = internal_calibration_ * external_calibration_.topLeftCorner<3, 3>();
}

void Camera::setInternalCalibration(
		const Eigen::Matrix3d& internal_calibration){
	internal_calibration_ = internal_calibration;
}

void Camera::setExternalCalibration(
		const Eigen::Matrix4d& external_calibration){
	external_calibration_ = external_calibration;
}

bool Camera::project3dPoint(const Eigen::Matrix3Xd& points,
		                    Eigen::Matrix2Xd* projected_points,
		                    Eigen::Matrix<double, 2, 3>* point_derivative)const{
	CHECK_NOTNULL(projected_points);
	bool success = true;
	projected_points->resize(2, points.cols());
	Eigen::Matrix3Xd transformed3d_points;

	transformToCameraFrame(points, &transformed3d_points);

	transformed3d_points = internal_calibration_ * transformed3d_points;

	for(int row = 0; row < 2; ++row){
		for(int col = 0; col < points.cols(); ++col){
			(*projected_points)(row, col) =
			  transformed3d_points(row, col)/transformed3d_points(2, col);
			if(transformed3d_points(2, col) <= 0){
				LOG(ERROR) << "Point behind camera.";
				success = false;
			}
		}
	}

	Eigen::Matrix<double, 2, 3> derivative_of_projection;
	derivative_of_projection <<
      transformed3d_points(2, 0), 0,                          -transformed3d_points(0, 0),
      0,                          transformed3d_points(2, 0), -transformed3d_points(1, 0);
	derivative_of_projection =
			derivative_of_projection/(transformed3d_points(2, 0) *
					                  transformed3d_points(2, 0));

	(*point_derivative) = derivative_of_projection * KR_;
	// Debug logging
	VLOG(3) << "transformed3d_points = " << transformed3d_points;
	return success;
}

void Camera::transformToCameraFrame(const Eigen::Matrix3Xd& points,
								    Eigen::Matrix3Xd* transformed_points)const{
	CHECK_NOTNULL(transformed_points);
	transformed_points->resize(3, points.cols());

	// Subtract camera center => add location of origin in camera
	(*transformed_points) = points +
	  external_calibration_.block<3, 1>(0, 3).replicate(1, points.cols());

	// Rotate
	(*transformed_points) =
			external_calibration_.block<3,3>(0, 0) * (*transformed_points);
}
