/*
 * kalman-filter.cc
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#include <glog/logging.h>

#include "kalman-filter-tracker/kalman-filter.h"

KalmanFilter::KalmanFilter(
		const unsigned long long int current_time_in_ms,
		std::shared_ptr<KalmanPropagtionBase>* propaget_filter)
: current_time_in_ms_(current_time_in_ms){
	CHECK_NOTNULL(propaget_filter);
	propaget_filter_ = *propaget_filter;
	// Also initializes state. For now high covar and zero pos.
	// TODO(Partha) : Improve this! Happens in propaget-marker-3d
	propaget_filter_->setStateAndCov(&state_, &state_cov_);
}

void KalmanFilter::estimeteState(const Eigen::MatrixXd& measurements,
		                         const unsigned long long int time_stamp_ms){
//	CHECK_GT(measurements(0, 0), current_time_in_ms_);
	if(time_stamp_ms < current_time_in_ms_){
		LOG(ERROR) <<
		  "Missed camera measurements because of late arrival. "
		  "current_time_in_ms_ = " << current_time_in_ms_ <<
		  " time_stamp_ms = " << time_stamp_ms;
		return;
	}
	int updator_id = measurements(2); // c_x, c_y, ID, so pick the last one

	mutex_for_update_filter_map_.lock();
	std::map<int, std::shared_ptr<KalmanUpdateBase> >::iterator it =
			update_filter_.find(updator_id);
	bool updator_notfound = (it == update_filter_.end());
	KalmanUpdateBase* current_updator = NULL;
	if(!updator_notfound){
		current_updator = it->second.get();
	}
	mutex_for_update_filter_map_.unlock();

	LOG(INFO) << "Current state = " << state_.transpose();
	VLOG(3) << "Current Co-var = " << std::endl << state_cov_;

	if(updator_notfound){
		LOG(ERROR) <<
		  "Updator have not been registered can not perform update with it";
	} else{
		// Propagation
		propaget_filter_->propagetStateAndCov(time_stamp_ms);

		// Updation
		const Eigen::MatrixXd blob_centre = measurements.block<2, 1>(0, 0);
		current_updator->updateStateAndCov(blob_centre);

		current_time_in_ms_ = time_stamp_ms;
		rviz_publisher_.log(state_, state_cov_);
	}
}

bool KalmanFilter::addNewUpdator(
		const int updator_id,
		std::shared_ptr<KalmanUpdateBase>* updator){
	CHECK_NOTNULL(updator);
	(*updator)->setStateAndCov(&state_, &state_cov_);
	mutex_for_update_filter_map_.lock();
	if(update_filter_.find(updator_id) != update_filter_.end()){
		LOG(ERROR) << "Attempt to re add same updator";
		mutex_for_update_filter_map_.unlock();
		return false;
	}
	update_filter_[updator_id] = *updator;
	mutex_for_update_filter_map_.unlock();
	return true;
}
