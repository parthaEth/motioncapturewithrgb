/*
 * main.cc
 *
 *  Created on: Sep 1, 2016
 *      Author: partha
 */

#include <glog/logging.h>
#include <gtest/gtest.h>
#include <ros/ros.h>

#include <server-side-com/connection-manager.h>

#include "kalman-filter-tracker/kalman-filter.h"
#include "kalman-filter-tracker/propaget-marker3d.h"
#include "kalman-filter-tracker/update-marker3d.h"

class ManageUpdators{
private:
	KalmanFilter* position_filter_;
public:
	ManageUpdators(KalmanFilter* position_filter)
		:position_filter_(position_filter){};

	void addNewClient(const Client& client){
		const int kImageRows = 480; //Isn't used for now
		const int kIageCols = 640; //Isn't used for now

		Camera camera(client.getInternalParams(), client.getExternalParams(),
					  kImageRows, kIageCols);
		std::shared_ptr<KalmanUpdateBase> kalman_updator =
					std::make_shared<UpdateMarker3D>(camera);
		position_filter_->addNewUpdator(client.getClientId(), &kalman_updator);
	}
};

int main(int argc, char **argv) {

	ros::init(argc, argv, "KalmanFilter_Main", ros::init_options::NoSigintHandler);
	// Initialize Google's logging library.
	FLAGS_logtostderr = 1;
	FLAGS_stderrthreshold = 0;
	google::InitGoogleLogging("Logging for kalman filter.");

	// Create a propagation method
	std::shared_ptr<KalmanPropagtionBase> kalman_propagator =
			std::make_shared<PropagetMarker3D>(0.0);

	KalmanFilter position_filter(0, &kalman_propagator);

	// Make arrangements for updators
	ManageUpdators update_manager(&position_filter);

	// Register add client call back & Launch connection manager
	std::function<void(const Eigen::MatrixXd&, const unsigned long long int)>
	  updateFunction = std::bind(&KalmanFilter::estimeteState,
					   	   	     &position_filter,
					   	   	     std::placeholders::_1, std::placeholders::_2);

	std::function<void(const Client&)> clientManagerFunction =
			std::bind(&ManageUpdators::addNewClient,
					  &update_manager,
					  std::placeholders::_1);
	ConnectionManager connction_manager(updateFunction, clientManagerFunction);
}
