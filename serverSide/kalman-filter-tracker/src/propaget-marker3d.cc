/*
 * propaget-marker3d.cc
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */

#include <gflags/gflags.h>

#include "kalman-filter-tracker/propaget-marker3d.h"
#include "kalman-filter-tracker/utility.h"

DEFINE_double(motion_model_noise, 0.01, "std of motion model for propagation in m^2");
DEFINE_double(propagation_noise, 0.01, "std of propagation model in m^2");

DEFINE_double(initial_position_x, 0.0, "Position_X to initialize marker");
DEFINE_double(initial_position_y, 0.0, "Position_Y to initialize marker");
DEFINE_double(initial_position_z, 0.0, "Position_Z to initialize marker");

DEFINE_double(initial_velocity_x, 0.0, "Velocity_X to initialize marker");
DEFINE_double(initial_velocity_y, 0.0, "Velocity_Y to initialize marker");
DEFINE_double(initial_velocity_z, 0.0, "Velocity_Z to initialize marker");

PropagetMarker3D::PropagetMarker3D(const double current_time_ms)
: KalmanPropagtionBase(NULL, NULL),
  current_time_ms_(current_time_ms){
	propagation_noise_.resize(kStateDimension, kStateDimension);
	propagation_noise_ =
			Eigen::MatrixXd::Identity(kStateDimension, kStateDimension) *
			FLAGS_propagation_noise;
	// Very less propagation uncertainty in velocity as there is no direct
	// update
	propagation_noise_.block<3, 3>(3, 3) *= 1e-4;

	motion_model_acc_noise_.resize(kVelocityDimension, kVelocityDimension);
	motion_model_acc_noise_ =
			Eigen::MatrixXd::Identity(kVelocityDimension, kVelocityDimension) *
			FLAGS_motion_model_noise;
}

void PropagetMarker3D::propagetStateAndCov(
		const unsigned long long int to_time_ms){
	if(current_time_ms_ == 0){
		// First call can not propaget
		current_time_ms_ = to_time_ms;
		return;
	}
	CHECK_GE(to_time_ms, current_time_ms_);
	// propaget covariance
	Eigen::MatrixXd state_derivative;
	getStateDerivative(to_time_ms, &state_derivative);
	// The propagation noise is made less if the time interval is small
	(*state_cov_) =
			state_derivative * (*state_cov_) * state_derivative.transpose() +
			propagation_noise_ * (to_time_ms - current_time_ms_) * 1e-1;

	// Propaget state
	constVelocityStatePropagation(to_time_ms);

	// After all the processes in propagation update the time stamp of state.
	current_time_ms_ = to_time_ms;
}

void PropagetMarker3D::getStateDerivative(
		const unsigned long long int to_time_ms,
		Eigen::MatrixXd* state_derivative){
	CHECK_GE(to_time_ms, current_time_ms_);
	CHECK_NOTNULL(state_derivative);
	state_derivative->resize(state_->rows(), state_->rows());

	// del_f_first3/del_pos = 1 and del_f_last3/del_vel = 1
	(*state_derivative) =
		Eigen::Matrix<double, kStateDimension, kStateDimension>::Identity();

	// del_f_fist3/del_vel = dt
	state_derivative->topRightCorner(kVelocityDimension, kVelocityDimension) =
		Eigen::Matrix3d::Identity() *
		  static_cast<double>(to_time_ms - current_time_ms_) * 1e-3;
}

void PropagetMarker3D::constVelocityStatePropagation(
		const unsigned long long int to_time_ms){
	CHECK_GE(to_time_ms, current_time_ms_);
	// TODO(Partha): Perhaps change to Runge–Kutta method, euler now.
	state_->block<3, 1>(0, 0) += (state_->block<3, 1>(3, 0) *
			static_cast<double>(to_time_ms - current_time_ms_) * 1e-3);

	Eigen::VectorXd acceleration;
	Utility::sampleFromMultiVariateGaussian(motion_model_acc_noise_,
			&acceleration);

	state_->block<3, 1>(3, 0) +=
			(acceleration * static_cast<double>(to_time_ms - current_time_ms_) *
					static_cast<double>(to_time_ms - current_time_ms_) * 1e-6);
}

void PropagetMarker3D::setStateAndCov(Eigen::MatrixXd* state,
		                              Eigen::MatrixXd* state_cov){
	CHECK_NOTNULL(state);
	CHECK_NOTNULL(state_cov);
	KalmanPropagtionBase::setStateAndCovPointer(state, state_cov);
	state->resize(kStateDimension, 1);
	state_cov->resize(kStateDimension, kStateDimension);
	// TODO(Partha): How to do better?
	// How to initialize marker state? For now initialize 0 with high covariance
	(*state) <<
	  FLAGS_initial_position_x, FLAGS_initial_position_y, FLAGS_initial_position_z,
	  FLAGS_initial_velocity_x, FLAGS_initial_velocity_y, FLAGS_initial_velocity_z;

	(*state_cov) =
		Eigen::Matrix<double, kStateDimension, kStateDimension>::Identity() *
			0.1; //Some high value
}
