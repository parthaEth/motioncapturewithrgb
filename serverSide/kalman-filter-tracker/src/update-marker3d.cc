/*
 * update-marker3d.cc
 *
 *  Created on: Aug 30, 2016
 *      Author: partha
 */

#include <gflags/gflags.h>
#include <glog/logging.h>

#include "kalman-filter-tracker/update-marker3d.h"

DEFINE_double(measurement_noise, 0.9, "std of propagation model in pixels^2");

UpdateMarker3D::UpdateMarker3D(const Camera& camera)
	: camera_(camera), KalmanUpdateBase(NULL, NULL){
	measurement_noise_ =
	  Eigen::MatrixXd::Identity(kMeasurementDimension, kMeasurementDimension) *
		FLAGS_measurement_noise;
}

void UpdateMarker3D::updateStateAndCov(
		const Eigen::MatrixXd blob_centre){
	if(!state_) LOG(FATAL) << "Call setStateAndCov first!";
	if(!state_cov_) LOG(FATAL) << "Call setStateAndCov first!";

	Eigen::Matrix2Xd predicted_blob_centre;
	Eigen::Matrix3Xd marker_position =
			state_->block<kPositionDimension, 1>(0, 0);
	Eigen::Matrix<double, kMeasurementDimension, kPositionDimension>
		position_derivative;

	if(camera_.project3dPoint(marker_position,
			               &predicted_blob_centre,
			               &position_derivative)){
		const Eigen::Matrix<double, kMeasurementDimension, 1>
		  innovation_residual = blob_centre - predicted_blob_centre;

		// Notice that the measurement derivative with respect to velocity is
		// zero.
		Eigen::Matrix<
		  double, kMeasurementDimension, kStateDimension>
		    measurement_derivative =
			  Eigen::Matrix<
			    double, kMeasurementDimension, kStateDimension>::Constant(0);
		measurement_derivative.block<kMeasurementDimension,
		                             kPositionDimension>(0, 0) =
		                            		 position_derivative;

		const Eigen::Matrix<
		  double, kMeasurementDimension, kMeasurementDimension>
		    innovation_covariace =
			  measurement_derivative * (*state_cov_) *
				measurement_derivative.transpose() + measurement_noise_;

		const Eigen::Matrix<
	      double, kStateDimension, kMeasurementDimension>
		    kalman_gain = (*state_cov_) * measurement_derivative.transpose() *
				innovation_covariace.inverse();
		// Debug logs
		VLOG(3) << "position_derivative = \n" << position_derivative;
		VLOG(3) << "innovation_covariace = \n " << innovation_covariace;
		// NAN Checking
		LOG_IF(FATAL,
				!((position_derivative - position_derivative).array() ==
				(position_derivative - position_derivative).array()).all());
		// Update state
		(*state_) += kalman_gain * innovation_residual;

		//Update state covar
		(*state_cov_) =
		  (Eigen::Matrix<double, kStateDimension, kStateDimension>::Identity() -
			kalman_gain * measurement_derivative) * (*state_cov_);

	}else{
		LOG(INFO) << "Point has gone behind camera! No update will happen";
	}
}

void UpdateMarker3D::setStateAndCov(Eigen::MatrixXd* state,
									Eigen::MatrixXd* state_cov){
	CHECK_NOTNULL(state);
	CHECK_NOTNULL(state_cov);
	KalmanUpdateBase::setStateAndCovPointer(state, state_cov);
}

