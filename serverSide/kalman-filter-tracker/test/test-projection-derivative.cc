/*
 * test-projectionDerivative.cc
 *
 *  Created on: Aug 29, 2016
 *      Author: partha
 */
#include <gtest/gtest.h>
#include <glog/logging.h>

#include "kalman-filter-tracker/camera.h"

TEST(kalman_filter_tracker, constantVelocutyPropagation) {
	// Find numerical derivative
	const Eigen::Matrix3d internal_calibration =
			Eigen::Matrix3d::Random() * 100;
	const Eigen::Matrix4d external_calibration = Eigen::Matrix4d::Random();
	const int image_rows = 480;
	const int iage_cols = 640;

	Camera camera(internal_calibration, external_calibration,
			      image_rows, iage_cols);

	Eigen::Matrix3Xd points(3, 1);
	Eigen::Matrix2Xd projected_points(2, 1), projected_points_disturbed(2, 1);

	Eigen::Matrix<double, 2, 3> point_derivative, fake_derivatives;
	Eigen::Matrix<double, 2, 3> point_derivative_num;

	const double kEpsilon = 1e-6;

	for(int i = 0; i < 10; ++i){
		points = Eigen::Matrix<double, 3, 1>::Random();
		camera.project3dPoint(points, &projected_points, &point_derivative);

		for(int col = 0; col < 3; ++col){
			points(col, 0) += kEpsilon; // The row index is col of derivative
			camera.project3dPoint(
					points, &projected_points_disturbed, &fake_derivatives);
			point_derivative_num.col(col) =
					(projected_points_disturbed - projected_points)/kEpsilon;
			points(col, 0) -= kEpsilon;
		}

		// Compare with returned derivative
		for(int row = 0; row < 2; ++row){
			for(int col = 0; col < 3; ++col){
				ASSERT_NEAR(point_derivative_num(row, col),
						    point_derivative(row, col), 1e-5);
			}
		}
	}
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ::google::InitGoogleLogging(argv[0]);
  return RUN_ALL_TESTS();
}


