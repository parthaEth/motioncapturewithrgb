/*
 * test-propagation-update.cc
 *
 *  Created on: Aug 30, 2016
 *      Author: partha
 */

#include <glog/logging.h>
#include <gtest/gtest.h>

#include "kalman-filter-tracker/kalman-filter.h"
#include "kalman-filter-tracker/propaget-marker3d.h"
#include "kalman-filter-tracker/update-marker3d.h"

TEST(kalman_filter_tracker, constantVelocutyPropagationTrivialUpdate) {

	// Create updators
	Eigen::Matrix3d internal_calibration = Eigen::Matrix3d::Random() * 100;
	Eigen::Matrix4d external_calibration = Eigen::Matrix4d::Random();
	const int image_rows = 480;
	const int iage_cols = 640;

	Camera camera0(internal_calibration, external_calibration,
			       image_rows, iage_cols);

	std::shared_ptr<KalmanUpdateBase> kalman_updator0 =
			std::make_shared<UpdateMarker3D>(camera0);

	//Second camera
	internal_calibration = Eigen::Matrix3d::Random() * 100;
	external_calibration = Eigen::Matrix4d::Random();

	Camera camera1(internal_calibration, external_calibration,
			       image_rows, iage_cols);

	std::shared_ptr<KalmanUpdateBase> kalman_updator1 =
			std::make_shared<UpdateMarker3D>(camera1);


	// Create a propagation method
	std::shared_ptr<KalmanPropagtionBase> kalman_propagator =
			std::make_shared<PropagetMarker3D>(0.0);

	// Create the filter with the above propagation and update
	KalmanFilter position_filter(0.0, 0, &kalman_propagator, &kalman_updator0);
	position_filter.addNewUpdator(1, &kalman_updator1);

	// Run the filter
	Eigen::MatrixXd measurement(4, 1);
	for(int i = 1; i < 50; ++i){
		measurement.block<2, 1>(1, 0) = Eigen::Matrix<double, 2, 1>::Random();
		measurement(0, 0) = i*100;
		measurement(3, 0) = i % 2;
		position_filter.estimeteState(measurement);
		LOG(ERROR) << "states = ";
		LOG(ERROR) << std::endl << position_filter.getState().transpose();
		LOG(ERROR) << std::endl << position_filter.getStateCov();
	}
}

int main(int argc, char **argv) {
	google::InitGoogleLogging("constantVelocutyPropagationTrivialUpdate.");
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
