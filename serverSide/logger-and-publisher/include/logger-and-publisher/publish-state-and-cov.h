/*
 * publish-state-and-cov.h
 *
 *  Created on: Sep 3, 2016
 *      Author: partha
 */

#ifndef RVIZ_PUBLISHER_PUBLISH_STATE_AND_COV_H_
#define RVIZ_PUBLISHER_PUBLISH_STATE_AND_COV_H_

#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

#include <Eigen/Dense>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

class LoggerAndPublisher{
private:
	ros::Publisher path_pub;
	ros::Publisher marker_pub;
	nav_msgs::Path path;
	visualization_msgs::Marker marker;
	ros::NodeHandle nh_;
	int marker_id_ = 0;
	std::queue<Eigen::MatrixXd> states_buffer_;
	std::queue<Eigen::MatrixXd> state_cov_buffer_;
	std::mutex buffer_mutex_;
	std::condition_variable new_data_in_buffer_condition_;
	std::string logfile_path_;
	bool end_logging_ = false;
	std::ofstream states_file_;
	std::ofstream states_cov_file_;
	std::thread logging_thread_;

public:
	LoggerAndPublisher();
	~LoggerAndPublisher();
	void logToCsvFile();
	void publishToRviz(const Eigen::MatrixXd& state,
			           const Eigen::MatrixXd& state_cov);
	void log(const Eigen::MatrixXd& state,
	         const Eigen::MatrixXd& state_cov);
	void handleLoggingBackEnd();
	void requestEndofLogging();

};




#endif /* RVIZ_PUBLISHER_PUBLISH_STATE_AND_COV_H_ */
