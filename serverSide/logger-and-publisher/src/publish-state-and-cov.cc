/*
 * publish-state-and-cov.cc
 *
 *  Created on: Sep 3, 2016
 *      Author: partha
 */

#include <gflags/gflags.h>
#include <glog/logging.h>

#include "logger-and-publisher/publish-state-and-cov.h"

DEFINE_bool(log_to_csv, true, "If the state and cov will be dumped as CSV");
DEFINE_bool(publish_to_rviz, true, "If the state and cov will be dumped as CSV");
DEFINE_string(log_file_path, "./",
		"Path to the folder where to put state and cov");

LoggerAndPublisher::LoggerAndPublisher(){
	path_pub = nh_.advertise<nav_msgs::Path>("marker_path",2);
	marker_pub = nh_.advertise<visualization_msgs::Marker>("marker_loc",2);
	if(FLAGS_log_to_csv){
		states_file_.open(FLAGS_log_file_path + "states.csv");
		states_cov_file_.open(FLAGS_log_file_path + "states_cov.csv");
	}
	logging_thread_ =
		std::thread(&LoggerAndPublisher::handleLoggingBackEnd, this);
}

LoggerAndPublisher::~LoggerAndPublisher(){
	if(FLAGS_log_to_csv){
		states_file_.close();
		states_cov_file_.close();
	}
}

void LoggerAndPublisher::handleLoggingBackEnd(){
	Eigen::MatrixXd state;
	Eigen::MatrixXd state_cov;

	while(true){
		std::unique_lock<std::mutex> lk(buffer_mutex_);
		if(end_logging_){
			lk.unlock();
			states_file_.close();
			states_cov_file_.close();
			return;
		}
		new_data_in_buffer_condition_.wait(lk);

		state = states_buffer_.front();
		states_buffer_.pop();

		state_cov = state_cov_buffer_.front();
		state_cov_buffer_.pop();

		lk.unlock();

		// Log to file
		if(FLAGS_log_to_csv){
			states_file_ << state.transpose() << std::endl;
			states_cov_file_ << state_cov << std::endl;
		}

		if(FLAGS_publish_to_rviz){
			publishToRviz(state, state_cov);
		}
	}
}

void LoggerAndPublisher::requestEndofLogging(){
	buffer_mutex_.lock();
	end_logging_ = true;
	buffer_mutex_.unlock();
}

void LoggerAndPublisher::log(const Eigen::MatrixXd& state,
		const Eigen::MatrixXd& state_cov){
	buffer_mutex_.lock();
	states_buffer_.push(state);
	state_cov_buffer_.push(state_cov);
	buffer_mutex_.unlock();
	new_data_in_buffer_condition_.notify_one();
}

void LoggerAndPublisher::publishToRviz(const Eigen::MatrixXd& state,
		const Eigen::MatrixXd& state_cov){
	//	path.header.stamp =ros::time::now();
	//	path.header.frame_id ="base_link";
	//	path_pub.publish(path);
	marker.header.frame_id = "world";
	marker.header.stamp = ros::Time::now();
	marker.ns = "my_namespace";
	marker.id = marker_id_++;
	marker.type = visualization_msgs::Marker::SPHERE;
	marker.action = visualization_msgs::Marker::ADD;
	marker.pose.position.x = state(0);
	marker.pose.position.y = state(1);
	marker.pose.position.z = state(2);
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
	marker.scale.x = 0.01;
	marker.scale.y = 0.01;
	marker.scale.z = 0.01;
	marker.color.a = 1.0; // Don't forget to set the alpha!
	marker.color.r = 1.0;
	marker.color.g = 0.0;
	marker.color.b = 0.0;
	marker_pub.publish(marker);
	ros::spinOnce();

//	LOG(INFO) << "Published to RVIZ";
}

