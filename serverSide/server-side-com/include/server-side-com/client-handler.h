#ifndef SERVER_SIDE_COM_CLIENT_HANDLER_H_
#define SERVER_SIDE_COM_CLIENT_HANDLER_H_

#include <condition_variable>
#include <mutex>
#include <vector>

#include <Eigen/Dense>
#include <glog/logging.h>

#include "server-side-com/utility.h"

class Client{
private:
	int client_id_ = 0;

	Eigen::Matrix<double, 4, 4> externa_params_;
	Eigen::Matrix<double, 3, 3> internal_params_;
	Eigen::Matrix<double, 4, 1> distortion_params_;

	std::mutex& data_mutex_;
	std::condition_variable& new_data_condition_;
	bool* notify_condition_var_;

	TcpConnectionManager socket_manager_;
	Utility desearilizer_;

	std::function<void(const Client&) >	clientRegistration_;

	std::mutex shut_down_interrupt_mutex_;
	bool shut_down_ = false;

public:
	static int client_id_generator;

	Client(const int accepted_socket,
		   std::function<void(const Client&) >	clientRegistration,
		   std::mutex& data_mutex,
		   std::condition_variable& new_data_condition,
		   bool* notify_condition_var)
			: data_mutex_(data_mutex),
			  new_data_condition_(new_data_condition),
			  clientRegistration_(clientRegistration),
			  notify_condition_var_(notify_condition_var){
		client_id_ = Client::client_id_generator++;
		socket_manager_ = TcpConnectionManager(accepted_socket);
		LOG_IF(FATAL, !socket_manager_.isConnected())
			<< "Provided an unconnected client.";
	}

	const Eigen::Matrix<double, 4, 4>& getExternalParams()const{
		return externa_params_;
	}
	const Eigen::Matrix<double, 3, 3>& getInternalParams()const{
		return internal_params_;
	}
	const Eigen::Matrix<double, 4, 1>& getDistortionParams()const{
		return distortion_params_;
	}

	const int getClientId() const {
		return client_id_;
	}

	const bool receiveInternalCalibration();
	const bool receiveExternalCalibration();
	const bool receiveDistortion();
	const bool receiveInternalExternalCalibration();
	const bool receiveInternalDistortionExternalCalibration();
	const void receiveCalibrationAndStartCollectingData(
			std::vector<Eigen::Matrix<double, 3, 1> >* measurements,
			std::vector<unsigned long long int >* time_stamps_ms);
	void shutDown(){
		shut_down_interrupt_mutex_.lock();
		shut_down_ = true;
		shut_down_interrupt_mutex_.unlock();
	}
};

#endif /*SERVER_SIDE_COM_CLIENT_HANDLER_H_*/
