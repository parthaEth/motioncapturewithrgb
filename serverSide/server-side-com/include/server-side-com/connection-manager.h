#ifndef SERVER_SIDE_COM_CONNECTION_MANAGER_H_
#define SERVER_SIDE_COM_CONNECTION_MANAGER_H_

#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>

#include <Eigen/Dense>

#include "server-side-com/client-handler.h"

class ConnectionManager{
private:
	int server_sock_;
	std::vector<std::shared_ptr<Client> > clients_;
	std::function< void(const Eigen::MatrixXd&,
			            const unsigned long long int time_stamp) >
		kalmanUpdateCallBack_;
	std::function<void(const Client&) > clientRegistration_;
	std::mutex data_mutex_;
	std::condition_variable new_data_condition_;
	bool notified_ = false;
	std::vector<Eigen::Matrix<double, 3, 1> > measurements_;
	std::vector<unsigned long long int > time_stamps_ms_;
	struct sockaddr_in serv_addr_, client_addr_;
	socklen_t clilen_;
	static std::mutex shut_down_interrupt_mutex_;
	static bool shut_down_;
	struct timeval timeout_;

public:
	ConnectionManager(std::function<void(const Eigen::MatrixXd&,
										 const unsigned long long int)>
					    kalmanUpdateCallBack,
					  std::function<void(const Client&)> clientRegistration);
	void logDataToFile(const std::string file_path);
	void listenForClients();
	void serveCallback();
	static void shutDownInterruptListener(int signum);
};

#endif /*SERVER_SIDE_COM_CONNECTION_MANAGER_H_*/
