/*
 * utility.h
 *
 *  Created on: Jul 21, 2016
 *      Author: partha
 */

#ifndef SERVER_SIDE_COM_UTILITY_H_
#define SERVER_SIDE_COM_UTILITY_H_

#include <arpa/inet.h>
#include <iostream>
#include <memory>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>


enum EndianNess{
	kBIG_ENDIAN,
	kLITTLE_ENDIAN
};

union FloatSearilizer{
	float num;
	unsigned char searilized_num[sizeof(float)];
};

class Utility{
private:
	FloatSearilizer float_searilizer_;

public:
	EndianNess detectEndianNessOfThisMachine();

	unsigned long long int charArrayToLongInt(const unsigned char* raw_msg,
			int offset,
			int num_bytes,
			EndianNess endian_ness);

	float charArrayToFloat(const unsigned char* raw_msg,
			int offset,
			EndianNess endian_ness);

	template<typename ParamType>
	int toCharArray(const ParamType data,
			int offset,
			const EndianNess endian_ness,
			unsigned char* array){
		const char* searilized_data_ptr =
				static_cast<const char*>(static_cast<const void*>(&data));
		if(endian_ness == detectEndianNessOfThisMachine()){
			for(int i = 0; i < sizeof(data); ++i){
				array[offset++] = searilized_data_ptr[i];
			}
		}else{
			for(int i = 0; i < sizeof(data); ++i){
				array[offset++] = searilized_data_ptr[sizeof(data) - i - 1];
			}
		}
		return offset;
	}
};

class TcpConnectionManager{
private:
	int socket_ = 0;
	sockaddr_in output_stream_;
	sockaddr_in input_stream_;
	struct hostent *hp_ = NULL;
	unsigned int length_ = 0;
	bool is_connected_ = false;

public:
	TcpConnectionManager(){};
	TcpConnectionManager(int socket):socket_(socket), is_connected_(true){}
	bool buildTcpReceiver(const char* server_ip, const int port);
	int send(const unsigned char* message_to_send, int msg_length) const;
	int receive(const int msg_length, unsigned char* message_received);
	void receiveExactLength(const int msg_length,
			unsigned char* message_received);
	void closeConnection();
	const bool isConnected() const;
};


#endif /* SERVER_SIDE_COM_UTILITY_H_ */
