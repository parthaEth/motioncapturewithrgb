#include <unistd.h>

#include <glog/logging.h>

#include "server-side-com/client-handler.h"

int Client::client_id_generator = 0;

const bool Client::receiveInternalCalibration(){
	const int kInternalParamLenght = 20;

	unsigned char internal_calibration_searilized[kInternalParamLenght];
	internal_params_ = Eigen::Matrix<double, 3, 3>::Identity();
	socket_manager_.receiveExactLength(kInternalParamLenght,
			                           &internal_calibration_searilized[0]);

	// Fill first row.
	for(int i = 0; i < 3; ++i){
		internal_params_(0, i) =
			desearilizer_.charArrayToFloat(&internal_calibration_searilized[0],
									  i*sizeof(float),
									  kBIG_ENDIAN);
	}

	// Fill second one now.
	for(int i = 1; i < 3; ++i){
		internal_params_(1, i) =
			desearilizer_.charArrayToFloat(&internal_calibration_searilized[0],
									  i*sizeof(float) + 2 * sizeof(float),
									  kBIG_ENDIAN);
	}
	LOG(INFO) << "Internal Calibration received for client = " << client_id_ <<
			     " as = " << std::endl << internal_params_;
	return true;
}

const bool Client::receiveExternalCalibration(){
	const int kExternalParamLength = 48;

	unsigned char external_calibration_searilized[kExternalParamLength];
	externa_params_ = Eigen::Matrix<double, 4, 4>::Identity();
	socket_manager_.receiveExactLength(kExternalParamLength,
			                           &external_calibration_searilized[0]);
	// Fill in the values row by row
	for(int row = 0; row < 3; ++row){
		for(int col = 0; col < 4; ++col){
			externa_params_(row, col) =
				desearilizer_.charArrayToFloat(&external_calibration_searilized[0],
											((row * 4) + col) * sizeof(float),
											kBIG_ENDIAN);
		}
	}
	LOG(INFO) << "External Calibration received for client = " << client_id_ <<
				 " as = " << std::endl << externa_params_;
	return true;
}

const bool Client::receiveDistortion(){
	LOG(FATAL) << "Remove this!!! receiveDistortion called. Implement!";
	return true;
}

const bool Client::receiveInternalExternalCalibration(){
	// Order of function call matters in the following for proper working; First
    // receiveInternalCalibration() will be called.
	return receiveInternalCalibration() & receiveExternalCalibration();
}

const bool Client::receiveInternalDistortionExternalCalibration(){
	// Order of function call matter in the following code; First
	// receiveDistortion() will be called.
	return receiveDistortion() & receiveInternalExternalCalibration();
}

const void Client::receiveCalibrationAndStartCollectingData(
			std::vector<Eigen::Matrix<double, 3, 1> >* measurements,
			std::vector<unsigned long long int >* time_stamps_ms){
	CHECK(receiveInternalCalibration() & receiveExternalCalibration());
	Eigen::Matrix<double, 3, 1> measurement_received;

	clientRegistration_(*this);
	const int kMeasurementSizeByes = 16; // 8 + 4 + 4
	unsigned char measurement_searilized[kMeasurementSizeByes];
	unsigned long long int time_stamp;
	while(true){
		// Handling shutdown
		shut_down_interrupt_mutex_.lock();
		if(shut_down_){
			shut_down_interrupt_mutex_.unlock();
			break;
		}
		shut_down_interrupt_mutex_.unlock();

		// Receive over TCP here
		socket_manager_.receiveExactLength(kMeasurementSizeByes,
				                           &measurement_searilized[0]);
		time_stamp =
		    desearilizer_.charArrayToLongInt(&measurement_searilized[0],
		    		                         0,
		    		                         sizeof(unsigned long long int),
		    		                         kBIG_ENDIAN);

		VLOG(3) << "Current timestamp of camera "<<client_id_<<" = "
				<< time_stamp;

		for(int i = 0; i < 2; ++i){ // 2 floats for Blob center
			measurement_received(i) =
				desearilizer_.charArrayToFloat(
					&measurement_searilized[0],
				    sizeof(time_stamp) + i*sizeof(float),
					kBIG_ENDIAN);
		}
		VLOG(3) << "Measurement_received = " << measurement_received;
		if(!((measurement_received - measurement_received).array() ==
			(measurement_received - measurement_received).array()).all()){
			LOG(ERROR) << "NAN in the measurements received"
					   << measurement_received;
			continue;
		}
		measurement_received(2) = client_id_;
		data_mutex_.lock();
		measurements->push_back(measurement_received);
		time_stamps_ms->push_back(time_stamp);
		*notify_condition_var_ = true;
		data_mutex_.unlock();
		new_data_condition_.notify_one();
	}
	socket_manager_.closeConnection();
}
