#include <signal.h>
#include <thread>

#include <gflags/gflags.h>

#include "server-side-com/connection-manager.h"

DEFINE_int32(port_number, 3000, "To specify Server port");
DEFINE_int32(max_camera_number, 50, "To specify Server port");

bool ConnectionManager::shut_down_ = false;
std::mutex ConnectionManager::shut_down_interrupt_mutex_;

void ConnectionManager::shutDownInterruptListener(int signum){
	LOG(INFO) << "Shutting down";
	shut_down_interrupt_mutex_.lock();
	shut_down_ = true;
	shut_down_interrupt_mutex_.unlock();
}

ConnectionManager::ConnectionManager(
		std::function<void(const Eigen::MatrixXd&,
				           const unsigned long long int)>
                             kalmanUpdateCallBack,
		std::function<void(const Client&)> clientRegistration)
:kalmanUpdateCallBack_(kalmanUpdateCallBack),
 clientRegistration_(clientRegistration){
	timeout_.tv_sec = 1;
	timeout_.tv_usec = 0;
	server_sock_= socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(server_sock_, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout_,
	                sizeof(timeout_));

	if (server_sock_ < 0) {
		LOG(FATAL) << "Can not open socket!";
	}

	bzero((char *) &serv_addr_, sizeof(serv_addr_));
	serv_addr_.sin_family = AF_INET;
	serv_addr_.sin_addr.s_addr = INADDR_ANY;
	serv_addr_.sin_port = htons(FLAGS_port_number);

	if (bind(server_sock_, (struct sockaddr *) &serv_addr_,
			sizeof(serv_addr_)) < 0) {
		LOG(FATAL)<< "Can not perform binding on port " << FLAGS_port_number;
	}
	listen(server_sock_, FLAGS_max_camera_number);
	clilen_ = sizeof(client_addr_);
	LOG(INFO) << "Listening on port " << FLAGS_port_number;

	// Register interrupt callback
	signal(SIGINT, ConnectionManager::shutDownInterruptListener);

	// Start listening for new clients
	std::thread t(&ConnectionManager::listenForClients, this);

	//serve callbacks with current active thread
	serveCallback();
}

void ConnectionManager::serveCallback(){
	while(true){
		std::unique_lock<std::mutex> lk(data_mutex_);
		while(!notified_){ // Handling spurious wakeup
			new_data_condition_.wait(lk);
		}
		notified_ = false;
		// Handling shutdown
		shut_down_interrupt_mutex_.lock();
		if(shut_down_){
			shut_down_interrupt_mutex_.unlock();
			lk.unlock();
			LOG(INFO) << "Connection manager callback terminated.";
			return;
		}
		shut_down_interrupt_mutex_.unlock();

		if(!measurements_.empty()){
			kalmanUpdateCallBack_(measurements_.back(), time_stamps_ms_.back());
		}
		lk.unlock();
	}
}

void ConnectionManager::listenForClients(){
	int accepted_socket = 0;
	while(true){
		// Handling shutdown
		shut_down_interrupt_mutex_.lock();
		if(shut_down_){
			shut_down_interrupt_mutex_.unlock();
			break;
		}
		shut_down_interrupt_mutex_.unlock();

		accepted_socket = accept(server_sock_, (struct sockaddr *)&client_addr_,
				&clilen_);
		if(accepted_socket < 0){
			continue;
		}

		LOG(INFO) << "New client connected";
		CHECK_GE(accepted_socket, 0) << "ERROR on accept";
		clients_.push_back(
				std::make_shared<Client>(
						accepted_socket,
						clientRegistration_,
						data_mutex_,
						new_data_condition_,
						&notified_));

		// Let this client now run on a seperate thread
		std::thread t(&Client::receiveCalibrationAndStartCollectingData,
				clients_.back().get(), &measurements_, &time_stamps_ms_);
		t.detach();
	}

	// Handling shutdown
	for(int client_index = 0; client_index < clients_.size(); ++client_index){
		clients_.at(client_index)->shutDown();
	}
	close(server_sock_);
	notified_ = true;
	new_data_condition_.notify_all(); // Wake the call back for termination
	LOG(INFO) << "Connection manager client listener ended";
}
