/*
 * Utility.cpp
 *
 *  Created on: Jul 21, 2016
 *      Author: partha
 */

#include <iostream>
#include <stdlib.h>
#include <sys/time.h>

#include <glog/logging.h>

#include "server-side-com/utility.h"

EndianNess Utility::detectEndianNessOfThisMachine(){
	int i = 1;
	unsigned char* buff = static_cast<unsigned char*>(static_cast<void*>(&i));
	if(buff[0] == 1 && buff[3] == 0){
		return kLITTLE_ENDIAN;
	}else if(buff[3] == 1 && buff[1] == 0){
		return kBIG_ENDIAN;
	}else{
		LOG(FATAL)<<"Fatal error: Do not understand endian ness of this computer"<<
				i<<" is represented as "<<(int)buff[0] << ", "<<(int)buff[1] << ", "
				<<(int)buff[2] << ", "<<(int)buff[3] << " which is neither big" <<
				" nor little endian."<<std::endl;
		return kBIG_ENDIAN; // This will never happen.
	}
}

unsigned long long int Utility::charArrayToLongInt(const unsigned char* raw_msg,
		int offset,
		int num_bytes,
		EndianNess endian_ness){
	if(num_bytes > 8){
		std::cout<< "Possible overflow while converting char array to long int"
				<< std::endl;
	}
	unsigned long long int result = 0;
	if(endian_ness == kLITTLE_ENDIAN){
		for(int i = 0; i < num_bytes; ++i){
			result |= (static_cast<unsigned long long int>(raw_msg[offset + i])
					    << (i*8));
		}
	}else{
		for(int i = 0; i < num_bytes; ++i){
			result |= (static_cast<unsigned long long int>(raw_msg[offset + i])
					    << ((num_bytes - i -1)*8));
		}
	}
	return result;
}

float Utility::charArrayToFloat(const unsigned char* raw_msg,
				int offset,
				EndianNess endian_ness){
	EndianNess machine_endian_ness = detectEndianNessOfThisMachine();
	if(endian_ness != machine_endian_ness){
		for(int i = 0; i < sizeof(float); ++i){
			float_searilizer_.searilized_num[i] =
					raw_msg[offset - i + sizeof(float) - 1]; // flipping bytes
		}
		return float_searilizer_.num;
	}else{
		return *((float*)(&raw_msg[offset]));
	}
}

// tcpConnectionManager
bool TcpConnectionManager::buildTcpReceiver(const char* server_ip, const int port){

	socket_ = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_ < 0){
		std::cout<<"Error:Socket could not be opened"<<std::endl;
		return false;
	}

	output_stream_.sin_family = AF_INET;
	hp_ = gethostbyname(server_ip);
	if (hp_ == NULL) {
		std::cout<<"Unknown host"<<std::endl;
		return false;
	}
	bcopy((char *)hp_->h_addr, (char *)&output_stream_.sin_addr,
		  hp_->h_length);
	output_stream_.sin_port = htons(port);
	length_ = sizeof(struct sockaddr_in);
	return true;
}

int TcpConnectionManager::send(const unsigned char* message_to_send, int msg_length) const{
	return sendto(socket_, message_to_send,
			      msg_length, 0, (const struct sockaddr *)&output_stream_, length_);
}

int TcpConnectionManager::receive(const int msg_length, unsigned char* message_received){
	return recvfrom(socket_, message_received, msg_length, 0,
			        (struct sockaddr *)&input_stream_, &length_);

}

void TcpConnectionManager::receiveExactLength(const int msg_length,
		                                      unsigned char* message_received){
	int bytes_got_so_far = 0;
	while(bytes_got_so_far < msg_length){
		int bytes_this_itr = 0;
		bytes_this_itr =
			receive(msg_length - bytes_got_so_far,
			        &message_received[bytes_got_so_far]);
		if(bytes_this_itr > 0){
			bytes_got_so_far += bytes_this_itr;
		}else if(bytes_this_itr == 0){
			usleep(1e5);
//			LOG(ERROR) << "Client dropped out!"; // TODO(Partha):Handle nicely
		}
	}
}

void TcpConnectionManager::closeConnection(){
	try{
		close(socket_);
	}catch(...){
		LOG(ERROR) << "Failed to close socket!";
	}
}

const bool TcpConnectionManager::isConnected() const {
	return is_connected_;
}

