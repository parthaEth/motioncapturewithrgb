/*
 * test-byte-to-long-Int.cc
 *
 *  Created on: Sep 6, 2016
 *      Author: partha
 */
#include <glog/logging.h>
#include <gtest/gtest.h>
#include <iostream>

#include "server-side-com/utility.h"

TEST(server_side_com, byteArrayToLongInt) {
	unsigned char a[] = {0, 0, 1, 86, 255, 209, 34, 168 };
	Utility desearilizer;
	ASSERT_EQ(1473170711208,
			  desearilizer.charArrayToLongInt(&a[0], 0, 8, kBIG_ENDIAN));
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

