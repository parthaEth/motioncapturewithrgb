#include <gtest/gtest.h>

#include "server-side-com/client-handler.h"
#include "server-side-com/connection-manager.h"

void fakeKalManUpdateFunction(const Eigen::MatrixXd& measurements){
	LOG(INFO)<< "Latest Measurement = " << measurements.transpose();
}

TEST(server_side_com, handlingMultipleConnections) {
	std::function<void(const Eigen::MatrixXd& )> fakeUpdateFunction
			= fakeKalManUpdateFunction;
	ConnectionManager connction_manager(fakeUpdateFunction);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
